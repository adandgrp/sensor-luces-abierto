
#include <MD_Parola.h>
#include <MD_MAX72xx.h>
#include <SPI.h>

#define HARDWARE_TYPE MD_MAX72XX::FC16_HW
#define MAX_DEVICES 4
#define CS_PIN 3

const int PIRPin= 1;
const int RELAY_PIN = 7;

const int PIRPinregadera= 2;
const int PIRPintoliete= 8;
const int RELAY_PINregadera = 9;
  int temp1 = 0;

MD_Parola myDisplay = MD_Parola(HARDWARE_TYPE, CS_PIN, MAX_DEVICES);

 
void setup()
{
 
  pinMode(LED_BUILTIN, OUTPUT);
 
  pinMode(PIRPin, INPUT_PULLUP);
  pinMode(RELAY_PIN, OUTPUT);
 
  pinMode(PIRPinregadera, INPUT_PULLUP);
  pinMode(PIRPintoliete, INPUT_PULLUP);
  pinMode(RELAY_PINregadera, OUTPUT);

  myDisplay.begin();
  myDisplay.setIntensity(0);
  myDisplay.displayClear();

  myDisplay.displayText("Ocupado"  , PA_CENTER, 100, 0, PA_SCROLL_LEFT, PA_SCROLL_LEFT);


 

}
 
void loop()
{
  int value= digitalRead(PIRPin);
  int valueregadera= digitalRead(PIRPinregadera);
  int valuetoliete= digitalRead(PIRPintoliete);

   
  Serial.println('iniciar');
 
  Serial.println(value);
   
  if (value == HIGH)
  {
    digitalWrite(RELAY_PIN, HIGH);
    digitalWrite(LED_BUILTIN, HIGH);
      Serial.println(value);
  }
  else
  {
    digitalWrite(LED_BUILTIN, LOW);
    digitalWrite(RELAY_PIN, LOW);
  }

if (valueregadera == HIGH)
  {
    digitalWrite(RELAY_PINregadera, HIGH);
      Serial.println(valueregadera);
    if (myDisplay.displayAnimate()) {
      myDisplay.displayReset();
    }      

  }

 if (valueregadera == LOW)
  {
    if(temp1 == 24000){
        temp1 = 0;
        Serial.println(valueregadera);
        Serial.println(temp1);
        digitalWrite(LED_BUILTIN, LOW);
        digitalWrite(RELAY_PINregadera, LOW);
        myDisplay.displayClear();
    }else{
      temp1 ++;
    }
  }
 
}
